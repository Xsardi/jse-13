package ru.t1.tbobkov.tm.api.repository;

import ru.t1.tbobkov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
